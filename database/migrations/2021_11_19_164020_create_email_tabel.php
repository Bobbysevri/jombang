<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmailTabel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_tabel', function (Blueprint $table) {
            $table->id();
            $table->string('status');
            $table->bigInteger('nip')->nullable();
            $table->bigInteger('nik')->nullable();
            $table->string('unit_id');
            $table->string('username');
            $table->string('password');
            $table->string('nama_lengkap');
            $table->bigInteger('telepon')->nullable();
            $table->string('foto')->nullable();
            $table->string('dokumen')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_tabel');
    }
}
