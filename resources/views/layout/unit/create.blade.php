@extends('layout.index')

@section('content')
<div class="content-body">
   <div class="card">
      @if(count($errors)>0)
      @foreach($errors->all() as $error)
      <div class="alert alert-danger" role="alert">
          {{ $error }}
      </div>
      @endforeach
      @endif

      @if(Session::has('success'))
      <div class="alert alert-success" role="alert">
          {{ Session('success') }}
      </div>

      @endif
       <div class="card-content">
           <div class="card-body card-dashboard">
               <div class="col-md-12 col-12">
                   <div class="card">
                       <div class="card-header">
                           <h4 class="card-title">Tambah Unit/OPD/Desa</h4>
                       </div>
                       <div class="card-content">
                           <div class="card-body">
                               <form class="form form-horizontal" action="{{ route('unit.store') }}" method="POST">
                                 @csrf
                                   <div class="form-body">
                                       <div class="row">

                                           <div class="col-12">
                                               <div class="form-group row">
                                                   <div class="col-md-4">
                                                       <span>Organisasi/OPD/Desa</span>
                                                   </div>
                                                   <div class="col-md-8">
                                                       <div class="position-relative has-icon-left">
                                                           <input type="text" id="nama_unit" class="form-control"
                                                               name="nama_unit" placeholder="Organisasi/OPD/Desa">
                                                           <div class="form-control-position">
                                                               <i class="feather icon-user"></i>
                                                           </div>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                          
                                           <div class="col-md-8 offset-md-4">
                                               <button type="submit" class="btn btn-primary mr-1 mb-1">Simpan</button>
                                               
                                           </div>
                                       </div>
                                   </div>
                               </form>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </div>
</div>
@endsection
