<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DummyData extends Model
{
    protected $table = 'datadummy';
}
