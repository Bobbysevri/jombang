@extends('backend.layout.index')
@push('css-vendor')
<link rel="stylesheet" type="text/css" href="{{ url ('app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
@endpush
@push('css-page')
@endpush
@section('content')
<div class="content-body">
    <div class="card">
        @if(count($errors)>0)
        @foreach($errors->all() as $error)
        <div class="alert alert-danger" role="alert">
            {{ $error }}
        </div>
        @endforeach
        @endif

        @if(Session::has('success'))
        <div class="alert alert-success" role="alert">
            {{ Session('success') }}
        </div>
        @endif
        <div class="card-header">
            <h4 class="card-title">Daftar Email</h4>
            {{-- <a href="{{ route('email.create')}}" type="submit" class="btn btn-primary mr-1 mb-1">Tambah</a> --}}
        </div>
        <div class="card-content">
            <div class="card-body card-dashboard">
                <p class="card-text">Pemerintah Kabupaten Jombang</p>
                <div class="table-responsive">
                    <table class="table nowrap zero-configuration" style="text-align: center">
                        <thead>
                            <tr>
                                <th width="50px">No</th>
                                <th width="200px">Nama</th>
                                <th width="300px">Email</th>
                                <th width="300px">Unit/OPD/desa</th>
                                {{-- <th width="200px">Aksi</th>  --}}

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($email as $result => $hasil)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $hasil->nama_lengkap }}</td>
                                <td>{{ $hasil->email }}</td>
                                <td>{{ $hasil->opd  ? $hasil->opd : '-'}}</td>
                                {{-- <td>
                                    <form action="{{ route('email.destroy', $hasil->id )}}" method="POST">
                                        <button type="submit" class=" btn btn-danger btn-sm">Hapus</button>

                                        <a href="{{ route('email.edit', $hasil->id ) }}"
                                            class="btn btn-warning btn-sm">Ubah</a>


                                        @csrf
                                        @method('delete')
                                    </form>
                                </td> --}}
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{-- {{ $email->links() }} --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js-vendor')
<script src="{{ url('app-assets/vendors/js/tables/datatable/pdfmake.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/vfs_fonts.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/buttons.html5.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/buttons.print.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js')}}"></script>
@endpush
@push('js-page')
<script src="{{ url('app-assets/js/scripts/datatables/datatable.js')}}"></script>
@endpush

