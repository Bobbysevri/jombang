@extends('layout.index2')
@section('content')
<div class="col-md-7 mx-auto text-center mb-3">
    <h1>
        Pemerintah Kabupaten Jombang<br>
    </h1>
    <h4>
        Aplikasi Registrasi Email & Domain Pemerintah Kabupaten Jombang<br>
        Klik tombol login untuk login Aplikasi Registrasi lalu kelola email & domain.
    </h4>
    @if(Session::has('success'))
    <div class="alert alert-success" role="alert">
        {{ Session('success') }}
    </div>
    @elseif(Session::has('error'))
    <div class="alert alert-danger" role="alert">
        {{ Session('error') }}
    </div>
    @endif
</div>
<div class="content-body">
    <section class="row flexbox-container">
        <div class="col-xl-8 col-11 d-flex justify-content-center mx-auto">
            <div class="card card-dashboard">
                <div class="row m-0" style="background-color: white">
                    <div class="col-lg-6 d-lg-block d-none text-center align-self-center px-1 py-0"
                        style="background-color: white">
                        <img src="{{ url('app-assets/images/pages/Logo-Kabupaten.jpg')}}" alt="branding logo" height="100%">
                    </div>
                    <div class="col-lg-6 col-12 p-0">
                        <div class="card rounded-0 mb-0 px-2">
                            <div class="card-header pb-1">
                                <div class="card-title">
                                    <h4 class="mb-0">Login Email</h4>
                                </div>
                            </div>
                            <p class="px-2">Pemerintah Kabupaten
                                Jombang</p>
                            <div class="card-content">
                                <div class="card-body pt-1">
                                    <form action="{{route('sinkron.login')}}" method="post">
                                        @csrf
                                        <fieldset class="form-label-group form-group position-relative has-icon-left">
                                            <input type="text" class="form-control" id="user-name"
                                                placeholder="Username" required name="username"
                                                value="{{old('username')}}">
                                            <div class="form-control-position">
                                                <i class="feather icon-user"></i>
                                            </div>
                                            <label for="user-name">Email</label>
                                        </fieldset>

                                        <fieldset class="form-label-group position-relative has-icon-left">
                                            <input type="password" class="form-control" id="user-password"
                                                placeholder="Password" required name="password">
                                            <div class="form-control-position">
                                                <i class="feather icon-lock"></i>
                                            </div>
                                            <label for="user-password">Password</label>
                                        </fieldset>
                                        <a href="{{ route('email.create')}}"
                                            class="btn btn-outline-primary float-left btn-inline">Register</a>
                                        <button type="submit"
                                            class="btn btn-primary float-right btn-inline">Login</button>
                                    </form>
                                </div>
                            </div>
                            {{-- <div class="login-footer">
                                <div class="divider">
                                    <div class="divider-text"><a href="{{route('faq')}}">Butuh bantuan ?</a></div>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
