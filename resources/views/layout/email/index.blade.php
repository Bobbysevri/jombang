@extends('layout.index')
@push('css-vendor')
<link rel="stylesheet" type="text/css" href="{{ url ('app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
@endpush
@push('css-page')
<link rel="stylesheet" type="text/css" href="{{url('app-assets/vendors/css/extensions/sweetalert2.min.css')}}">
@endpush
@section('content')
<div class="content-body">
    <div class="row">
        <div class="col-lg-3 col-sm-6 col-12">
            <div class="card">
                <div class="card-header d-flex align-items-start pb-0">
                    <div>
                        <h2 class="text-bold-700 mb-0">{{count($email)}}</h2>
                        <p>Email terdaftar</p>
                    </div>
                    <div class="avatar bg-rgba-success p-50 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-mail text-success font-medium-5"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6 col-12">
            <div class="card">
                <div class="card-header d-flex align-items-start pb-0">
                    <div>
                        <h2 class="text-bold-700 mb-0">{{7992-count($email)}}</h2>
                        <p>Belum terdaftar</p>
                    </div>
                    <div class="avatar bg-rgba-danger p-50 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-mail text-danger font-medium-5"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Daftar Email</h4>
            {{-- <a href="{{ route('email.create')}}" type="submit" class="btn btn-primary mr-1 mb-1">Tambah</a> --}}
        </div>
        <div class="card-content">
            <div class="card-body card-dashboard">
                @if(Session::has('success'))
                <div class="alert alert-success" role="alert">
                    {{ Session('success') }}
                </div>
                @elseif(Session::has('error'))
                <div class="alert alert-danger" role="alert">
                    {{ Session('error') }}
                </div>
                @endif
                <p class="card-text">Pemerintah Kabupaten Jombang</p>
                <div class="table-responsive">
                    <table class="table nowrap zero-configuration" style="text-align: center">
                        <thead>
                            <tr>
                                <th width="50px">No</th>
                                <th width="200px">Nama</th>
                                <th width="300px">Email</th>
                                <th width="300px">Unit/OPD/desa</th>
                                <th width="300px">KTP</th>
                                {{-- <th width="200px">Aksi</th> --}}

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($email as $result => $hasil)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $hasil->nama_lengkap }}</td>
                                <td>{{ $hasil->email }}</td>
                                <td>{{ $hasil->opd  ? $hasil->opd : '-'}}</td>
                                <td><a href="@if(!empty($hasil->path_ktp)) {{url($hasil->path_ktp)}} @endif"
                                        target="_blank"
                                        class="btn btn-primary btn-sm @if(empty($hasil->path_ktp)) disabled @endif  ">Download</a>
                                </td>
                                {{-- <td>
                                    <button type="button" data-id={{$hasil->id}}
                                class="btn btn-icon btn-danger mr-1 mb-1 confirm-alert"><i
                                    class="feather icon-trash"></i></button>
                                <a href="{{ route('users.edit', $hasil->id ) }}"
                                    class="btn btn-icon btn-warning mr-1 mb-1"><i class="feather icon-edit"></i></a>
                                <form id="delete{{$hasil->id}}" action="{{ route('email.destroy', $hasil->id )}}"
                                    method="POST">
                                    @csrf
                                    @method('delete')
                                </form>
                                </td> --}}
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{-- {{ $email->links() }} --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js-vendor')
<script src="{{ url('app-assets/vendors/js/tables/datatable/pdfmake.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/vfs_fonts.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/buttons.html5.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/buttons.print.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js')}}"></script>
@endpush
@push('js-page')
{{-- <script src="{{ url('app-assets/js/scripts/datatables/datatable.js')}}"></script> --}}
<script src="{{ url('app-assets/vendors/js/extensions/sweetalert2.all.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/my.js')}}"></script>
@endpush
