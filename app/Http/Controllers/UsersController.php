<?php

namespace App\Http\Controllers;

use App\User as User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function index()
    {
        try {
            $users = User::latest()->get();
        } catch (Exception $th) {
            Log::error($e->getMessage());
        }
        return view('backend.users.index', compact('users'));

    }
    public function create()
    {
        return view('backend.users.add');
    }
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|min:4',
            'email' => 'required|unique:App\User,email|email:rfc,dns|',
            'password' => 'required|min:6',
        ]);
        try {
            $dataInsert = [
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password'=> Hash::make($request->input('password')),
            ];
            $insert=User::create($dataInsert);
        } catch (\Throwable $th) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Data tidak tersimpan');
        }
        return redirect()->route('users.index')->with('success', 'Data disimpan');

    }
    public function edit($id)
    {
        try {
            $idUser = $id;
            $user = User::where('id', $idUser)->first();
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Data tidak berubah');
        }
        return view('backend.users.edit', compact('user'));
    }
    public function update(Request $request, $id)
    {
        try {
            $dataUpdated = [
                'name' => $request->input('name'),
                'email' => $request->input('email'),
            ];
            if ($request->input('password')) {
                $dataUpdated['password'] = Hash::make($request->input('password'));
            }
            $update = User::where('id', $id)->update($dataUpdated);
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Data tidak berubah');
        }
        return redirect()->route('users.index')->with('success', 'Data berubah');

    }
    public function destroy($id)
    {
        try {

            $delete=User::where('id',$id)->delete();
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Data tidak terhapus');
        }
        return redirect()->back()->with('success', 'Data dihapus');

    }
}
