@extends('layout.index')
@push('css-vendor')
@endpush
@push('css-page')
@endpush
@section('content')
<div class="content-body">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">User Admin</h4>
            {{-- <a href="{{ route('email.create')}}" type="submit" class="btn btn-primary mr-1 mb-1">Tambah</a> --}}
        </div>
        <div class="card-content">
            <div class="card-body card-dashboard">
                @if(Session::has('success'))
                <div class="alert alert-success" role="alert">
                    {{ Session('success') }}
                </div>
                @elseif(Session::has('error'))
                <div class="alert alert-success" role="alert">
                    {{ Session('error') }}
                </div>
                @endif
                <form class="form form-horizontal" method="post" action="{{route('users.update',$user->id)}}">
                    @csrf
                    @method('PUT')
                    <div class="form-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <span>Nama</span>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" id="first-name" class="form-control" name="name"
                                            placeholder="First Name" value="{{$user->name}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <span>Email</span>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="email" id="email-id" class="form-control" name="email"
                                            placeholder="Email" value="{{$user->email}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <span>Password</span>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="password" id="password" class="form-control" name="password"
                                            placeholder="Password">
                                            <p><small class="text-muted">Kosongkan jika tidak update password</small></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary mr-1 mb-1">Perbarui</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js-vendor')
@endpush
@push('js-page')
@endpush
