$(document).ready(function () {
    $('.zero-configuration').DataTable();
    $('.confirm-alert').on('click', function (e) {
        id = e.target.dataset.id
        Swal.fire({
            title: 'Anda yakin ?',
            text: "Data akan di hapus",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-danger ml-1',
            buttonsStyling: false,
        }).then(function (result) {
            if (result.value) {
                $("#delete" + id).submit()
            }

        })
    });

})
