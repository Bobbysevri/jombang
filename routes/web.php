<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('email-login');
});
Auth::routes(['register' => false]);
// Route::get('/home', 'HomeController@index')->name('home');
Route::view('/email-login', 'email-login')->name('login.email');
// Route::view('/faq', 'faq')->name('faq');

// Route::view('/email-login-dash', 'email-login-dash')->name('login.email.dash');
Route::view('/registrasi', 'registrasi');
//REGISTRASI EMAIL
Route::resource('/email', 'RegisterEmailController');

//ROUTE ADMINISTRATOR
Route::middleware('auth')->group(function () {
    Route::resource('/users', 'UsersController');
});
Route::middleware('auth')->prefix('backend')->group(function () {
    Route::get('/daftar-email', 'Backend\EmailController@index')->name('backend.daftar-email');
});
//ROUTE SINKRON
Route::prefix('sinkron')->group(function () {
    Route::post('/login-email', 'RegisterEmailController@preAuth')->name('sinkron.login');
    //DEVELOP ONLY
    // Route::get('/auth', 'SinkronController@loginMail')->name('sinkron.auth');
    // Route::get('/auth-user', 'SinkronController@loginUserMail')->name('sinkron.user.auth');
    // Route::get('/create-account', 'SinkronController@createAccountEmail')->name('sinkron.create.account');
    // Route::get('/preauth', 'SinkronController@preAuth')->name('sinkron.preauth');

});
//UNUSER ROUTE
// Route::get('/email', 'RegisterEmailController@index');
// Route::get('/emailnon', 'RegisterEmailController@index');
// Route::view('/email-login2', 'email-login2');
// Route::resource('/unit', 'MasterUnitController')->middleware('auth');
