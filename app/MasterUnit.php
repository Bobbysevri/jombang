<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterUnit extends Model
{
    protected $fillable = ['nama_unit'];

    protected $table = 'unit';

    public function email()
    {
        return $this->hasMany('App\Email');
    }
}
