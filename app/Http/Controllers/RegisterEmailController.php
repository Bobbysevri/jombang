<?php

namespace App\Http\Controllers;

use App\Email;
use Exception;
use Illuminate\Http\Client\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Crypt;
use Log;

class RegisterEmailController extends Controller
{
    protected $user;
    protected $password;
    protected $serverZimbra;

    public function __construct()
    {
        $this->user = env('ZIMBRA_API_USER');
        $this->password = env('ZIMBRA_API_PASSWORD');
        $this->serverZimbra = env('ZIMBRA_SERVER');
        $this->middleware('auth')->only(['index', 'destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getNip(string $nip)
    {
        $uri = env('NIP_URI');
        $token = env('NIP_TOKEN');
        $getNIP = Http::get($uri, [
            'reqToken' => $token,
            'nip' => $nip,
        ]);
        $listNIP = $getNIP->json();
        $datas = [
            'status' => $getNIP->status(),
            'data' => $listNIP['result'],
        ];
        return $datas;
    }
    private function loginMail()
    {
        $xml = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:urn='urn:zimbraAdmin' xmlns:urn1='urn:zimbraAdmin'>
            <soapenv:Header>
                <urn:context xmlns:urn='urn:zimbra'/>
            </soapenv:Header>
            <soapenv:Body>
                <urn1:AuthRequest xmlns:urn1='urn:zimbraAdmin'>
                    <urn1:account by='name'>" . $this->user . "</urn1:account>
                    <urn1:password>" . $this->password . "</urn1:password>
                </urn1:AuthRequest>
            </soapenv:Body>
            </soapenv:Envelope>";
        try {
            $response = Http::withHeaders([
                'Content-Type' => 'text/xml; charset=utf-8',
            ])->withBody($xml, 'text/xml')->post($this->serverZimbra);
            $responseAuth = $response->cookies()->getCookieByName('ZM_ADMIN_AUTH_TOKEN');
            $data = [
                'token' => $responseAuth->getValue(),
                'domain' => $responseAuth->getDomain(),
                'expires' => $responseAuth->getExpires(),
                'status' => $response->status(),
            ];
            Log::info("Sinkron email berhasil", $data);
            return $data;
        } catch (\Throwable $th) {
            $data = [
                'status' => 400,
                'message' => "sinkron email gagal",
                'error' => $th->getMessage(),
                'line' => $th->getLine(),
            ];
            Log::error('Error Sinkron', $data);
            return $data;
            //return response()->json($data, 400);
        }
    }
    private function createAccountEmail($request = null)
    {
        try {
            // dd($request);
            $data = $request;
            $auth = $this->loginMail();
            if ($auth['status'] != 200) {
                throw new Exception($auth['error'], 400);
            }
            $xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:zimbraAdmin" xmlns:urn1="urn:zimbraAccount">
            <soapenv:Header>
                <context xmlns="urn:zimbra">
                    <authToken>' . $auth['token'] . '</authToken>
                </context>
            </soapenv:Header>
            <soapenv:Body>
                    <CreateAccountRequest xmlns="urn:zimbraAdmin">
                        <name>' . $data['email'] . '</name>
                        <password>' . $data['password'] . '</password>
                        <a n="givenName">' . $data['nama_lengkap'] . '</a>
                        <a n="sn">' . $data['nama_lengkap'] . '</a>
                        <a n="displayName">' . $data['nama_lengkap'] . '</a>
                    </CreateAccountRequest>
            </soapenv:Body>
            </soapenv:Envelope>';
            $response = Http::withHeaders([
                'Content-Type' => 'text/xml; charset=utf-8',
            ])->withBody($xml, 'text/xml')->post($this->serverZimbra);
            $data = [
                'status' => $response->status(),
                'body' => $response->body(),
            ];
        } catch (\Throwable $th) {
            $data = [
                'status' => 400,
                'message' => "sinkron email gagal",
                'error' => $th->getMessage(),
                'line' => $th->getLine(),
                'file' => $th->getFile(),
            ];
            Log::error('Error Sinkron', $data);
        }
        return $data;
    }

    public function index()
    {
        $email = Email::latest()->get();
        $data = DB::table('datadummy')->get();
        return view('layout.email.index', compact(['email', 'data']));
        // return abort(404);
    }
    public function loadData(Request $request)
    {

        $nip = $request->nip;
        $data = DB::table('datadummy')->where('nip', 'like', "%" . $nip . "%")->get();
        return response($data);
        // dd($data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // $uriOpd = 'http://103.142.14.56:3000/web/api/data/opd';
        // $getOpd = Http::get($uriOpd);
        // $masterOpd = $getOpd->json();
        $nip = $request->nip;

        $status = false;
        $data = [];
        if ($request->input('nip')) {
            $nip = $this->getNip($request->input('nip'));
            if (empty($nip['data'][0]['NIP_BARU'])) {
                return redirect()->back()->withInput()->with('error', 'NIP tidak ditemukan');
            } else {
                $status = true;
                $data = [
                    'opd' => $nip['data'][0]['OPD_INDUK'],
                    'nip' => $nip['data'][0]['NIP_BARU'],
                    'nama' => $nip['data'][0]['NAMA_LENGKAP'],
                    'password' => 'foobar',
                    'email' => str_replace([" ", "'", "`"], "", strtolower($nip['data'][0]['NAMA_LENGKAP'])) . '@jombangkab.go.id',
                ];
            }
            $data = (object) $data;
            return view('layout.email.create', compact('data', 'nip', 'status'));
        } else {
            return view('layout.email.create', compact('data', 'status'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'status' => 'nullable',
            'opd' => 'nullable',
            // 'nip' => 'required',
            'username' => 'required',
            'email' => 'required',
            // 'gambar' => 'required|dimensions:max_width=800,max_height=450',
            'ktp' => 'nullable|mimes:jpg,jpeg|max:2048',
            'password' => 'required',
            'nama_lengkap' => 'required',
            'telepon' => 'nullable',
            'dokumen' => 'nullable',
        ]);
        $sinkron = $this->createAccountEmail($request->all());
        if ($sinkron['status'] == 200) {
            $email = Email::create([
                'opd' => $request->opd,
                'username' => $request->username,
                'nama_lengkap' => $request->nama_lengkap,
                'nik' => $request->nik,
                'email' => $request->email,
                'password' => Crypt::encryptString($request->password),
                'telepon' => $request->telepon,
                'nip' => $request->nip_pegawai,
            ]);
            //UPLOAD FILE KTP
            if ($request->hasFile('ktp')) {
                $regKtp = $request->file('ktp');
                $namaKtp = time() . '-' . $regKtp->getClientOriginalName();
                $upload = $regKtp->move('upload/ktp/', $namaKtp);
                $email['path_ktp']='upload/ktp/' . $namaKtp;
            }
            $email->save();
            return redirect()->back()->with('status', 'Email berhasil dibuat');
        } else {
            return redirect()->back()->with('error', 'Email sudah ada');
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $delete = Email::where('id', $id)->delete();
        } catch (Exception $e) {
            Log::error($e->getMessage());
            return redirect()->back()->with('error', 'Data tidak terhapus');
        }
        return redirect()->back()->with('success', 'Data dihapus');

    }
    protected function loginUserMail($email, $password)
    {
        //SINKRON EMAIL SERVER
        $xml = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:urn='urn:zimbraAccount' xmlns:urn1='urn:zimbraAccount'>
            <soapenv:Header>
                <urn:context xmlns:urn='urn:zimbra'/>
            </soapenv:Header>
            <soapenv:Body>
                <urn1:AuthRequest xmlns:urn1='urn:zimbraAccount'>
                    <urn1:account by='name'>" . $email . "</urn1:account>
                    <urn1:password>" . $password . "</urn1:password>
                </urn1:AuthRequest>
            </soapenv:Body>
            </soapenv:Envelope>";
        $response = Http::withHeaders([
            'Content-Type' => 'text/xml; charset=utf-8',
        ])->withBody($xml, 'text/xml')->post($this->serverZimbra);
        $data = [
            'status' => $response->status(),
        ];
        $responseAuth = $response->cookies()->getCookieByName('ZM_AUTH_TOKEN');
        if ($responseAuth) {
            $data = [
                'token' => $responseAuth->getValue(),
                'domain' => $responseAuth->getDomain(),
                'expires' => $responseAuth->getExpires(),
                'status' => $response->status(),
            ];
        }
        Log::info("Login email ", $data);
        return $data;
    }
    public function preAuth(Request $request)
    {
        try {
            $PREAUTH_KEY = env('ZIMBRA_PREAUTH_TOKEN');
            $WEB_MAIL_PREAUTH_URL = env('ZIMBRA_PREAUTH_URI');
            $email = $request->input('username');
            $password = $request->input('password');
            $login = $this->loginUserMail($email, $password);
            if ($login['status'] == 200) {
                if (empty($PREAUTH_KEY)) {
                    Log::error("Need preauth key for domain ");
                }
                $timestamp = time() * 1000;
                $preauthToken = hash_hmac("sha1", $email . "|name|0|" . $timestamp, $PREAUTH_KEY);
                $preauthURL = $WEB_MAIL_PREAUTH_URL . "?account=" . $email . "&timestamp=" . $timestamp . "&expires=0&preauth=" . $preauthToken;
                return redirect()->away($preauthURL);
            } else {
                return redirect()->back()->withInput()->with('error', 'Email atau password tidak cocok');
            }
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            $data = [
                'status' => 400,
                'message' => "Login email gagal",
                'trace' => $th->getMessage(),
            ];
            return redirect()->back()->with('error', $data['message']);
        }
    }
}
