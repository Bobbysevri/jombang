<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $guarded = [];

    protected $table = 'email';

    public function unit()
    {
        return $this->belongsTo('App\MasterUnit');
    }
}
