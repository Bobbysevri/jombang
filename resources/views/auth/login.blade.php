@extends('layout.index')
@section('content')
<div class="col-md-7 mx-auto text-center mb-3">
    @if(Session::has('success'))
    <div class="alert alert-success" role="alert">
        {{ Session('success') }}
    </div>
    @elseif(Session::has('error'))
    <div class="alert alert-danger" role="alert">
        {{ Session('error') }}
    </div>
    @endif
</div>
<div class="content-body">
    <section class="row flexbox-container">
        <div class="col-xl-4 col-4 d-flex justify-content-center mx-auto">
            <div class="card card-body">
                <div class="row m-0" style="background-color: white">
                    <div class="col-lg-12 col-12 p-0">
                        <div class="card rounded-0 mb-0 px-2">
                            <div class="card-header pb-1">
                                <div class="card-title">
                                    <h4 class="mb-0">Login User</h4>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body pt-1">
                                    <form action="{{route('login')}}" method="post">
                                        @csrf
                                        <fieldset class="form-label-group form-group position-relative has-icon-left">
                                            <input type="text" class="form-control @error('email') is-invalid @enderror" id="user-name"
                                                placeholder="Username" required name="email" value="{{old('email')}}">
                                            <div class="form-control-position">
                                                <i class="feather icon-user"></i>
                                            </div>
                                            <label for="user-name">Email</label>
                                            @error('email')
                                            <div class="invalid-feedback">
                                                    {{$message}}
                                                </div>
                                            @enderror
                                        </fieldset>

                                        <fieldset class="form-label-group position-relative has-icon-left">
                                            <input type="password" class="form-control" id="user-password"
                                                placeholder="Password" required name="password">
                                            <div class="form-control-position">
                                                <i class="feather icon-lock"></i>
                                            </div>
                                            <label for="user-password">Password</label>
                                        </fieldset>
                                        <button type="submit" class="btn btn-primary btn-block">Login</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
