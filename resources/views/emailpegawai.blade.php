@extends('layout.index')
@push('css-vendor')
<link rel="stylesheet" type="text/css" href="{{ url ('app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
@endpush
@push('css-page')
@endpush
@section('content')
<div class="card">
    <div class="card-header">
        <h4 class="card-title">Cek Email Pegawai</h4>
    </div>
    <div class="card-content">
        <div class="card-body">
            <p class="mt-1">Masukan <code>Nomor Induk Pegawai</code> </p>
            <form class="form-horizontal" novalidate>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="controls">
                                <input type="number" name="text" class="form-control" placeholder="NIP" required
                                    data-validation-required-message="This First Name field is required">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-primary">Cek</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table zero-configuration" width="100%">
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th >Nama</th>
                                        <th>Email</th>
                                        <th>Office</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>1</th>
                                        <td>boby</td>
                                        <td>boby@mail.com</td>
                                        <td>Janti</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@push('js-vendor')
<script src="{{ url('app-assets/vendors/js/tables/datatable/pdfmake.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/vfs_fonts.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/buttons.html5.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/buttons.print.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js')}}"></script>
@endpush
@push('js-page')
<script src="{{ url('app-assets/js/scripts/datatables/datatable.js')}}"></script>
@endpush
