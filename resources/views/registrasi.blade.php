@extends('layout.index')
@push('css-vendor')
<link rel="stylesheet" type="text/css" href="{{ url ('app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
@endpush
@push('css-page')
@endpush
@section('content')
<div class="content-body">
    <div class="card">
        <div class="card-content">
            <div class="card-body card-dashboard">
                <div class="col-md-12 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Form Pendaftaran Email Pemerintah Daerah Jombang</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <form class="form form-horizontal">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <div class="position-relative has-icon-left">
                                                            <input type="number" id="fname-icon" class="form-control"
                                                                name="fname-icon" placeholder="Nomer Induk Pegawai">
                                                            <div class="form-control-position">
                                                                <i class="feather icon-user"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="position-relative has-icon-left">
                                                            <input type="number" id="fname-icon" class="form-control"
                                                                name="fname-icon"
                                                                placeholder="Nomer Induk Kependudukan">
                                                            <div class="form-control-position">
                                                                <i class="feather icon-user"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <button type="submit" class="btn btn-primary mr-1 mb-1">Cek</button>
                                                </div>
                                                 <hr>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Organisasi/OPD/Desa</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="position-relative has-icon-left">
                                                            <input type="text" id="fname-icon" class="form-control"
                                                                name="fname-icon" placeholder="Organisasi/OPD/Desa">
                                                            <div class="form-control-position">
                                                                <i class="feather icon-user"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Email</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="position-relative has-icon-left">
                                                            <input type="email" id="email-icon" class="form-control"
                                                                name="email-id-icon" placeholder="Email">
                                                            <div class="form-control-position">
                                                                <i class="feather icon-mail"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Username</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="position-relative has-icon-left">
                                                            <input type="number" id="contact-icon" class="form-control"
                                                                name="contact-icon" placeholder="Username">
                                                            <div class="form-control-position">
                                                                <i class="feather icon-smartphone"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Password</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="position-relative has-icon-left">
                                                            <input type="password" id="pass-icon" class="form-control"
                                                                name="contact-icon" placeholder="Password">
                                                            <div class="form-control-position">
                                                                <i class="feather icon-lock"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Nama Lengkap</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="position-relative has-icon-left">
                                                            <input type="number" id="contact-icon" class="form-control"
                                                                name="contact-icon" placeholder="Nama Lengkap">
                                                            <div class="form-control-position">
                                                                <i class="feather icon-smartphone"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>Nomer telepon</span>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="position-relative has-icon-left">
                                                            <input type="number" id="contact-icon" class="form-control"
                                                                name="contact-icon" placeholder="Nomer Telepon">
                                                            <div class="form-control-position">
                                                                <i class="feather icon-smartphone"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>File Upload KTP/Kartu Pegawai</span>
                                                    </div>
                                                    <div class="col-md-8">

                                                        <input type="file" id="contact-icon" class="form-control"
                                                            name="contact-icon" placeholder="File Upload">


                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group row">
                                                    <div class="col-md-4">
                                                        <span>File Upload Tambahan</span>
                                                    </div>
                                                    <div class="col-md-8">

                                                        <input type="file" id="contact-icon" class="form-control"
                                                            name="contact-icon" placeholder="File Upload">


                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8 offset-md-4">
                                                <button type="submit" class="btn btn-primary mr-1 mb-1">Simpan</button>
                                                <button type="reset"
                                                    class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js-vendor')
<script src="{{ url('app-assets/vendors/js/tables/datatable/pdfmake.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/vfs_fonts.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/buttons.html5.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/buttons.print.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js')}}"></script>
@endpush
@push('js-page')
<script src="{{ url('app-assets/js/scripts/datatables/datatable.js')}}"></script>
@endpush
