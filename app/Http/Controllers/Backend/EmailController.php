<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Email;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Log;

class EmailController extends Controller
{
    public function index()
    {
        $email = Email::latest()->get();
        return view('backend.email.index', compact(['email']));

    }
}
