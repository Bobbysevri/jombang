<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use Log;

class SinkronController extends Controller
{
    protected $user;
    protected $password;
    protected $serverZimbra;
    public function __construct()
    {
        $this->user = env('ZIMBRA_API_USER');
        $this->password = env('ZIMBRA_API_PASSWORD');
        $this->serverZimbra = env('ZIMBRA_SERVER');
    }
    public function index()
    {
        try {
            $url = 'https://webmail.jombangkab.go.id:7071/home/admin/sent/?fmt=json';
            $response = Http::withBasicAuth(env('ZIMBRA_API_USER'), env('ZIMBRA_API_PASSWORD'))->timeout(5)->get($url);
            $sentEmail = $response->json();
            Log::info('Sinkron berhasil');
            return response()->json($sentEmail, 200);
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            $data = [
                'status' => 400,
                'message' => "sinkron email gagal",
                'trace' => $th->getMessage(),
            ];
            return response()->json($data, 400);

        }
    }
    public function loginMail()
    {
        $xml = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:urn='urn:zimbraAdmin' xmlns:urn1='urn:zimbraAdmin'>
            <soapenv:Header>
                <urn:context xmlns:urn='urn:zimbra'/>
            </soapenv:Header>
            <soapenv:Body>
                <urn1:AuthRequest xmlns:urn1='urn:zimbraAdmin'>
                    <urn1:account by='name'>" . $this->user . "</urn1:account>
                    <urn1:password>" . $this->password . "</urn1:password>
                </urn1:AuthRequest>
            </soapenv:Body>
            </soapenv:Envelope>";
        try {
            $response = Http::withHeaders([
                'Content-Type' => 'text/xml; charset=utf-8',
            ])->withBody($xml, 'text/xml')->post($this->serverZimbra);
            $responseAuth = $response->cookies()->getCookieByName('ZM_ADMIN_AUTH_TOKEN');
            $data = [
                'token' => $responseAuth->getValue(),
                'domain' => $responseAuth->getDomain(),
                'expires' => $responseAuth->getExpires(),
                'status' => $response->status(),
            ];
            Log::info("Sinkron email berhasil", $data);
            return $data;
        } catch (\Throwable $th) {
            $data = [
                'status' => 400,
                'message' => "sinkron email gagal",
                'error' => $th->getMessage(),
                'line' => $th->getLine(),
            ];
            Log::error('Error Sinkron', $data);
            return $data;
            //return response()->json($data, 400);
        }
    }
    public function loginUserMail()
    {
        $xml = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:urn='urn:zimbraAccount' xmlns:urn1='urn:zimbraAccount'>
            <soapenv:Header>
                <urn:context xmlns:urn='urn:zimbra'/>
            </soapenv:Header>
            <soapenv:Body>
                <urn1:AuthRequest xmlns:urn1='urn:zimbraAccount'>
                    <urn1:account by='name'>dika@jombangkab.go.id</urn1:account>
                    <urn1:password>foobar</urn1:password>
                </urn1:AuthRequest>
            </soapenv:Body>
            </soapenv:Envelope>";
        try {
            $response = Http::withHeaders([
                'Content-Type' => 'text/xml; charset=utf-8',
            ])->withBody($xml, 'text/xml')->post($this->serverZimbra);
            $data = [
                'status'=>$response->status(),
            ];
            $responseAuth = $response->cookies()->getCookieByName('ZM_AUTH_TOKEN');
            if($responseAuth){
                $data = [
                    'token' => $responseAuth->getValue(),
                    'domain' => $responseAuth->getDomain(),
                    'expires' => $responseAuth->getExpires(),
                    'status' => $response->status(),
                ];
            }
            Log::info("Sinkron email berhasil", $data);
            return $data;
        } catch (\Throwable $th) {
            $data = [
                'status' => 400,
                'message' => "sinkron email gagal",
                'error' => $th->getMessage(),
                'line' => $th->getLine(),
            ];
            Log::error('Error Sinkron', $data);
            return $data;
        }
    }
    public function createAccountEmail()
    {
        try {
            $auth = $this->loginMail();
            if ($auth['status'] != 200) {
                throw new Exception($auth['error'], 400);
            }
            $xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:zimbraAdmin" xmlns:urn1="urn:zimbraAccount">
            <soapenv:Header>
                <context xmlns="urn:zimbra">
                    <authToken>' . $auth['token'] . '</authToken>
                </context>
            </soapenv:Header>
            <soapenv:Body>
                    <CreateAccountRequest xmlns="urn:zimbraAdmin">
                        <name>dika@jombangkab.go.id</name>
                        <password>foobar</password>
                        <a n="givenName">dika</a>
                        <a n="sn">dika</a>
                        <a n="displayName">dika</a>
                    </CreateAccountRequest>
            </soapenv:Body>
            </soapenv:Envelope>';
            $response = Http::withHeaders([
                'Content-Type' => 'text/xml; charset=utf-8',
            ])->withBody($xml, 'text/xml')->post($this->serverZimbra);
            $data = [
                'status' => $response->status(),
                'body' => $response->body(),
            ];
        } catch (\Throwable $th) {
            $data = [
                'status' => 400,
                'message' => "sinkron email gagal",
                'error' => $th->getMessage(),
                'line' => $th->getLine(),
                'file' => $th->getFile(),
            ];
            Log::error('Error Sinkron', $data);
        }
        if ($data['status'] == 200) {
            return redirect()->back()->with('status', 'Email dibuat');
        } else {
            return redirect()->back()->withErrors(['msg' => 'Gagal membuat email']);
        }
    }
    public function preAuth()
    {
        $PREAUTH_KEY = "db214ff7481d6925ddc4ca7e8914ad1c0b8b0e678d6fe255f338c05e6d91c133";
        $WEB_MAIL_PREAUTH_URL = "https://webmail.jombangkab.go.id/service/preauth";

    /**
     * User's email address and domain. In this example obtained from a GET query parameter.
     * i.e. preauthExample.php?email=user@domain.com&domain=domain.com
     * You could also parse the email instead of passing domain as a separate parameter
     */
        $user = 'dika';
        $domain ='jombangkab.go.id';

        $email = "{$user}@{$domain}";

        if (empty($PREAUTH_KEY)) {
            die("Need preauth key for domain " . $domain);
        }

    /**
     * Create preauth token and preauth URL
     */
        $timestamp = time() * 1000;
        $preauthToken = hash_hmac("sha1", $email . "|name|0|" . $timestamp, $PREAUTH_KEY);
        $preauthURL = $WEB_MAIL_PREAUTH_URL . "?account=" . $email . "&timestamp=" . $timestamp . "&expires=0&preauth=" . $preauthToken;

    /**
     * Redirect to Zimbra preauth URL
     */
    return redirect()->away($preauthURL);

        // dd($preauthURL);
        // return response()->json($preauthURL, 200);
        //header("Location: $preauthURL");

    }
}
