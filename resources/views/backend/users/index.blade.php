@extends('layout.index')
@push('css-vendor')
<link rel="stylesheet" type="text/css" href="{{ url ('app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
@endpush
@push('css-page')
<link rel="stylesheet" type="text/css" href="{{url('app-assets/vendors/css/extensions/sweetalert2.min.css')}}">
@endpush
@section('content')
<div class="content-body">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">User Admin</h4>
            {{-- <a href="{{ route('email.create')}}" type="submit" class="btn btn-primary mr-1 mb-1">Tambah</a> --}}
        </div>
        <div class="card-content">
            <div class="card-body card-dashboard">
                @if(Session::has('success'))
                <div class="alert alert-success" role="alert">
                    {{ Session('success') }}
                </div>
                @elseif(Session::has('error'))
                <div class="alert alert-danger" role="alert">
                    {{ Session('error') }}
                </div>
                @endif
                <a href="{{ route('users.create')}}" class="btn btn-primary mr-1 mb-1">Tambah</a>
                <div class="table-responsive">
                    <table class="table nowrap zero-configuration" style="text-align: center">
                        <thead>
                            <tr>
                                <th width="50px">No</th>
                                <th width="200px">Nama</th>
                                <th width="300px">Email</th>
                                <th width="200px">Aksi</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $hasil)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $hasil->name }}</td>
                                <td>{{ $hasil->email }}</td>
                                <td>
                                    <button type="button" data-id={{$hasil->id}}
                                        class="btn btn-icon btn-danger mr-1 mb-1 confirm-alert"><i
                                            class="feather icon-trash"></i></button>
                                    <a href="{{ route('users.edit', $hasil->id ) }}"
                                        class="btn btn-icon btn-warning mr-1 mb-1"><i class="feather icon-edit"></i></a>
                                    <form id="delete{{$hasil->id}}" action="{{ route('users.destroy', $hasil->id )}}"
                                        method="POST">
                                        @csrf
                                        @method('delete')
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{-- {{ $email->links() }} --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js-vendor')
<script src="{{ url('app-assets/vendors/js/tables/datatable/pdfmake.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/vfs_fonts.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/buttons.html5.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/buttons.print.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js')}}"></script>
@endpush
@push('js-page')
<script src="{{ url('app-assets/vendors/js/extensions/sweetalert2.all.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/my.js')}}"></script>
@endpush
