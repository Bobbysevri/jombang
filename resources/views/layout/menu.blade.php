<div class="header-navbar navbar-expand-sm navbar navbar-horizontal floating-nav navbar-light navbar-without-dd-arrow navbar-shadow menu-border"
    top="" role="navigation" data-menu="menu-wrapper">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto"><a class="navbar-brand"
                    href="{{ url ('html/ltr/horizontal-menu-template/index.html')}}">
                    <div class="brand-logo"></div>
                    <h2 class="brand-text mb-0">Jombang</h2>
                </a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i
                        class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i
                        class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary"
                        data-ticon="icon-disc"></i></a></li>
        </ul>
    </div>
    <!-- Horizontal menu content-->
    <div class="navbar-container main-menu-content" data-menu="menu-container">
        <!-- include ../../../includes/mixins-->
        <ul class="nav navbar-nav " id="main-menu-navigation" data-menu="menu-navigation">
            @auth
            <li class="{{ Request::is('email')?'active':''}}"><a href="{{url ('email')}}"><i
                        class="feather icon-mail"></i><span data-i18n="Apps">Daftar Email</span></a>
            </li>
            @endauth
            <li class="{{ Request::is('email-login')?'active':''}}"><a href="{{url ('email-login')}}"><i
                class="feather icon-lock"></i><span data-i18n="Others">Login Email</span></a>

    </li>
            @guest
                
            @endguest
            
            @auth
                <li class="{{ Request::is('users')?'active':''}}"><a href="{{route('users.index')}}"><i
                            class="feather icon-users"></i><span data-i18n="Others">User</span></a>
                </li>
            <li style="position: absolute !important;
            right: 0;
            top: 0;"><a href="{{ route('logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();"><i class="feather icon-log-out"></i><span
                        data-i18n="Pages">Logout</span></a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </li>
            @endauth

        </ul>
    </div>
</div>
