@extends('layout.index')
@push('css-vendor')
<link rel="stylesheet" type="text/css" href="{{url ('app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{url ('app-assets/vendors/css/forms/select/select2.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{url ('assets/css/center-simple.css')}}">
@endpush
@push('css-page')
@endpush
@section('content')
<div class="content-body">
    <div class="card">
        <div class="card-content">
            <div class="card-body card-dashboard">
                <div class="col-md-12 col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Form Pendaftaran Email Pemerintah Daerah Jombang</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }} silahkan <a href="{{route('login.email')}}">Login</a>
                                </div>
                                @elseif(session('error'))
                                <div class="alert alert-danger">
                                    {{ session('error') }}
                                </div>
                                @endif
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                <form class="form form-horizontal" action="{{ route ('email.create')}}" method="get" >
                                    <div class="form-body">
                                        <div class="col-12">
                                            <div class="form-group ">
                                                <div class="col-md-4">
                                                    <div class="position-relative has-icon-left">
                                                        <input type="number" id="nip" class="form-control" name="nip"
                                                            placeholder="Nomer Induk Pegawai"
                                                            value="{{isset($data->nip) ? $data->nip:old('nip') }}">
                                                        <div class="form-control-position">
                                                            <i class="feather icon-shield"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                            <div class="col-md-4">
                                            <button type="submit" class="btn btn-primary mr-1 mb-1">Cek</button>
                                            <a href="{{route('login.email')}}">Sudah punya akun ?</a>
                                            </div>
                                        </div>
                                        </div>

                                    </div>
                                </form>
                                <form class="form form-horizontal" action="{{ route('email.store')}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-body">
                                        <div class="row">
                                            <hr>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Organisasi/OPD/Desa<span class="text-danger">*</span></span>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="position-relative has-icon-left">
                                                        <input type="text" class="form-control" name="opd"
                                                            placeholder="Organisasi/OPD/Desa" required readonly
                                                        value={{isset($data->opd) ? $data->opd:''}}>
                                                        <div class="form-control-position">
                                                            <i class="feather icon-tag"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 hidden">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>NIP Pegawai<span class="text-danger">*</span></span>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="position-relative has-icon-left">
                                                        <input type="text" class="form-control" name="nip_pegawai"
                                                            placeholder="NIP Pegawai" required
                                                            value="{{isset($data->nip) ? $data->nip:''}}">
                                                        <div class="form-control-position">
                                                            <i class="feather icon-shield"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Nama Lengkap<span class="text-danger">*</span></span>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="position-relative has-icon-left">
                                                        <input type="text" id="nama_lengkap" class="form-control"
                                                            name="nama_lengkap" placeholder="Nama Lengkap"
                                                            value="{{isset($data->nama) ? $data->nama:''}}">
                                                        <div class="form-control-position">
                                                            <i class="feather icon-user"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Email<span class="text-danger">*</span></span>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="position-relative has-icon-left">
                                                        <input type="email" id="email" class="form-control" name="email"
                                                            placeholder="Email" required
                                                            value="{{isset($data->email) ? $data->email:''}}">
                                                        <div class="form-control-position">
                                                            <i class="feather icon-mail"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Username<span class="text-danger">*</span></span>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="position-relative has-icon-left">
                                                        <input type="text" id="username" class="form-control"
                                                            name="username" placeholder="Username" required
                                                            value="{{isset($data->email) ? $data->email:''}}">
                                                        <div class="form-control-position">
                                                            <i class="feather icon-user-check"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Password<span class="text-danger">*</span></span>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="position-relative has-icon-left">
                                                        <input type="password" id="password" class="form-control"
                                                            name="password" placeholder="Password" required>
                                                        <div class="form-control-position">
                                                            <i class="feather icon-lock"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>Nomer telepon<span class="text-danger">*</span></span>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="position-relative has-icon-left">
                                                        <input type="number" id="telepon" class="form-control"
                                                            name="telepon" placeholder="Nomer Telepon"
                                                            value={{old('telepon')}} required>
                                                        <div class="form-control-position">
                                                            <i class="feather icon-smartphone"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <span>File Upload KTP/Kartu Pegawai</span>
                                                </div>
                                                <div class="col-md-8">
                                                    <input type="file" id="ktp" class="form-control" name="ktp"
                                                        placeholder="File Upload">
                                                        <p><small class="text-muted">Maksimal 2mb dengan format jpg/jpeg</small></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8 offset-md-4">
                                            @if($status)
                                            <button type="submit" class="btn btn-primary mr-1 mb-1">Simpan</button>
                                            @endif
                                            <button type="reset"
                                                class="btn btn-outline-warning mr-1 mb-1">Reset</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@push('js-vendor')
<script src="{{ url('app-assets/vendors/js/tables/datatable/pdfmake.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/vfs_fonts.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/buttons.html5.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/buttons.print.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
<script src="{{ url('app-assets/js/scripts/datatables/datatable.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/pace-js@latest/pace.min.js"></script>
@endpush
@push('js-page')
<script src="{{url('app-assets/js/scripts/forms/select/form-select2.js')}}"></script>
@endpush
