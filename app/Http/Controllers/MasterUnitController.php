<?php

namespace App\Http\Controllers;

use App\MasterUnit;
use Illuminate\Http\Request;

class MasterUnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $unit = MasterUnit::paginate(10);
        return view('layout.unit.index', compact('unit'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layout.unit.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_unit' => 'required|min:3'
        ]);


        $unit = MasterUnit::create([
            'nama_unit' => $request->nama_unit,

        ]);

        return redirect()->route('unit.index')->with('success', 'Unit berhasil disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $unit = MasterUnit::findorfail($id);
        return view('layout.unit.edit', compact('unit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_unit' => 'required'
        ]);

        $unit_data = [
            'nama_unit' => $request->nama_unit,

        ];

        MasterUnit::whereId($id)->update($unit_data);

        return redirect()->route('unit.index')->with('success', 'Data Berhasil di Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $unit = MasterUnit::findorfail($id);
        $unit->delete();

        return redirect()->back()->with('success', 'Data Berhasil Dihapus');
    }
}
