@extends('layout.index')
@push('css-vendor')
<link rel="stylesheet" type="text/css" href="{{ url ('app-assets/vendors/css/tables/datatable/datatables.min.css')}}">
@endpush
@push('css-page')
@endpush
@section('content')
<div class="content-body">
   <div class="card">
      <div class="card-header">
          <h4 class="card-title">Daftar Email Pegawai</h4>
      </div>
      <div class="card-content">
          <div class="card-body card-dashboard">
              <p class="card-text">Pemerintah Kabupaten Jombang</p>
              <div class="table-responsive">
                  <table class="table nowrap scroll-horizontal-vertical">
                      <thead>
                          <tr>
                              <th>Nama</th>
                              <th>Email</th>
                              <th>Office</th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr>
                                <td>boby</td>
                                <td>boby@mail.com</td>
                                <td>Janti</td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
  </div>
</div>
@endsection
@push('js-vendor')
<script src="{{ url('app-assets/vendors/js/tables/datatable/pdfmake.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/vfs_fonts.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/buttons.html5.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/buttons.print.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js')}}"></script>
<script src="{{ url('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js')}}"></script>
@endpush
@push('js-page')
<script src="{{ url('app-assets/js/scripts/datatables/datatable.js')}}"></script>
@endpush
